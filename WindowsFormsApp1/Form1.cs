﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListBox;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        List<string> words = new List<string>();
        List<string> matchingWords = new List<string>();
        StringComparer compare = StringComparer.CurrentCultureIgnoreCase;
        StringComparison c = StringComparison.CurrentCultureIgnoreCase;
        string currentHighlighted = "";
        string tmp = "";
        int li = 0; //listbox1 index
        //bool compare = true;
        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
            listBox1.Hide();
            richTextBox1.Select();
        }

        //private void Form1_Resize(object sender, System.EventArgs e)
        //{
        //    Control Form1 = (Control)sender;
            
        //}

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


       
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            string[] lines;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            //"c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        string ext = Path.GetExtension(openFileDialog1.FileName);
                        if (ext != ".txt")
                        {
                            MessageBox.Show("Error: Opened file doesn't have .txt extension.");
                            return;

                        }
                        using (myStream)
                        {
                            lines = File.ReadLines(openFileDialog1.FileName).ToArray();
                            words = new List<string>(lines);
                            words.Sort();
                            listBox2.DataSource = words;

                        }
                        myStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void reset1()
        {
            tmp = "";
            matchingWords = words;
            listBox1.Hide();
        }

        public void PreviewKeyDownEventHandler3(object listBox2_PreviewKeyDown, PreviewKeyDownEventArgs e) // usuwanie ze slownika
        {
            if (e.KeyCode == Keys.Delete)
            {
                {
                    if (words.Count > 0 && listBox2.SelectedIndex >= 0)
                    {
                        foreach (var toDelete in listBox2.SelectedItems)
                        {
                            words.Remove((string)toDelete);
                        }
                    }
                }
                if (listBox2.SelectedIndex >= words.Count)
                    listBox2.SelectedIndex = words.Count - 1;
                listBox2.DataSource = null;
                listBox2.DataSource = words;
            }
        }


        private void richTextBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (Char.IsLetterOrDigit((char)e.KeyValue))
            {
                tmp += (char)e.KeyValue;
                string curHigh = (listBox1.GetItemText(listBox1.SelectedItem));

                matchingWords = (words.Where(s => s.StartsWith(tmp,c))).ToList();
                matchingWords.Sort();
                if (matchingWords.Count == 0)
                {
                    reset1();
                }
                else
                {
                    Point p = richTextBox1.GetPositionFromCharIndex(richTextBox1.SelectionStart);
                    listBox1.Location = new Point(p.X - 5, p.Y + 60);
                    listBox1.DataSource = matchingWords;

                    if (curHigh.StartsWith(tmp, c)) // podswietlac ciagle ten sam wyraz
                    {
                        listBox1.SelectedIndex = listBox1.FindString(curHigh);
                    }
                    listBox1.Show();
                }
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (tmp != "")
                {
                    if (listBox1.SelectedIndex != matchingWords.Count - 1)
                        listBox1.SelectedIndex++;
                }
            }
            else if(e.KeyCode == Keys.Up)
            {
                if (tmp != "" && listBox1.SelectedIndex > 0)
                    listBox1.SelectedIndex--;
            }
            else if(e.KeyCode == Keys.Tab )
            {
                if (matchingWords.Count > 0 &&  tmp != "")
                {
                    richTextBox1.Text += (listBox1.GetItemText(listBox1.SelectedItem)).Substring(tmp.Length);
                    reset1();
                    richTextBox1.Select();
                    this.ActiveControl = this.richTextBox1;
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    SendKeys.Send("{TAB}");
                }
            }
            else if ((e.KeyCode == Keys.Left || e.KeyCode == Keys.Right))
            {
                reset1();
            }
               
            else 
            {
                reset1();

            }
            richTextBox1.Refresh();



        }
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream = null;

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.InitialDirectory = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = saveFileDialog1.OpenFile()) != null)
                    {
                        {
                            StreamWriter writer = new StreamWriter(myStream);
                            for (int i = 0; i < words.Count; ++i)
                                writer.WriteLine(words[i]);
                            myStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not save file to disk. Original error: " + ex.Message);
                }
            }

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBox2_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) //typeof(Html_Tool.PictureContainer)))
            {

                try
                {
                    String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
                    string[] lines;

                    System.IO.FileInfo fi = new System.IO.FileInfo(files[0]);

                    if (System.IO.File.Exists(fi.FullName) && fi.Extension == ".txt")
                    {
                        //tb.Text = System.IO.File.ReadAllText(fi.FullName);
                        //my
                        //using (myStream)
                        {
                            lines = File.ReadLines(fi.FullName).ToArray();
                            foreach (string s in lines)
                            {
                                if (!words.Contains(s))
                                    words.Add(s);
                            }
                            words.Sort();
                            listBox2.DataSource = null;
                            listBox2.DataSource = words;
                            //myStream.Close();
                        }

                    }

                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }
        

        private void nothing(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void button2_Click(object sender, EventArgs e) // sort
        {
            bool asc = true;
            for (int i = 0; i < words.Count - 1; ++i)
            {
                if (String.Compare(words[i], words[i+1], c) > 0)
                {
                    asc = false;
                    break;
                }
            }
            if (asc == false)
            {
                words.Sort();
                matchingWords.Sort();
            }
            else
            {
                words = words.OrderByDescending(i => i).ToList();
                matchingWords = matchingWords.OrderByDescending(i => i).ToList();

            }
            listBox2.DataSource = null; listBox2.DataSource = words;
            listBox1.DataSource = null; listBox1.DataSource = matchingWords;

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void boldButton_Click(object sender, EventArgs e) // BOLD
        {
            if (boldButton.Checked == true)
                richTextBox1.Font = new Font(richTextBox1.Font, richTextBox1.Font.Style | FontStyle.Bold);
            else
                richTextBox1.Font = new Font(richTextBox1.Font, richTextBox1.Font.Style & ~FontStyle.Bold);
        }

        private void italicButton_Click(object sender, EventArgs e)
        {
            if (italicButton.Checked == true)
                richTextBox1.Font = new Font(richTextBox1.Font, richTextBox1.Font.Style | FontStyle.Italic);
            else
                richTextBox1.Font = new Font(richTextBox1.Font, richTextBox1.Font.Style & ~FontStyle.Italic);          
        }

        private void ulineButton_Click(object sender, EventArgs e)
        {
            if (ulineButton.Checked == true)
                richTextBox1.Font = new Font(richTextBox1.Font, richTextBox1.Font.Style | FontStyle.Underline);
            else
                richTextBox1.Font = new Font(richTextBox1.Font, richTextBox1.Font.Style & ~FontStyle.Underline);
        }

        private void fColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            // Keeps the user from selecting a custom color.
            MyDialog.AllowFullOpen = true;
            // Allows the user to get help. (The default is false.)
            MyDialog.ShowHelp = true;
            // Sets the initial color select to the current text color.
            MyDialog.Color = richTextBox1.SelectionColor;

            // Update the text box color if the user clicks OK 
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.SelectionStart = 0;
                richTextBox1.SelectionLength = richTextBox1.Text.Length;
                richTextBox1.SelectionColor = MyDialog.Color;
                richTextBox1.SelectionLength = 0;
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
            }
        }

        private void bgColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            // Keeps the user from selecting a custom color.
            MyDialog.AllowFullOpen = true;
            // Allows the user to get help. (The default is false.)
            MyDialog.ShowHelp = true;
            // Sets the initial color select to the current text color.
            MyDialog.Color = richTextBox1.BackColor;

            // Update the text box color if the user clicks OK 
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.BackColor = MyDialog.Color;
                //richTextBox1.SelectionStart = 0;
                //richTextBox1.SelectionLength = richTextBox1.Text.Length;
                //richTextBox1.SelectionColor = MyDialog.Color;
                //richTextBox1.SelectionLength = 0;
                //richTextBox1.SelectionStart = richTextBox1.Text.Length;
            }
        }
    }
}
